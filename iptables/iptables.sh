#!/bin/bash

iptables -F
iptables -F -t nat
iptables -F -t mangle
iptables -F -t raw

iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -s 192.168.0.0/16 -p tcp --dport 22 -j ACCEPT

iptables -P INPUT DROP
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT

echo "----------------------"
iptables -nvL
echo "----------------------"